const electron = require('electron')

const app = electron.app
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const url = require('url')
const Positioner = require('electron-positioner')

let mainWindow

function createWindow () {
  mainWindow = new BrowserWindow({frame: false, kiosk: true})
  mainWindow.loadURL('http://www.marksandspencer.com')

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  app.quit
})

app.on('activate', function () {
})